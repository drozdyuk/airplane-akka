import com.typesafe.sbt.SbtMultiJvm
import com.typesafe.sbt.SbtMultiJvm.MultiJvmKeys.MultiJvm

val AkkaVersion = "2.3.2"
val ScalaVersion = "2.11.0"
val ScalaTestVersion =  "2.1.6"

lazy val multiJvmSettings = SbtMultiJvm.multiJvmSettings ++ Seq(
  // Make sure MultiJvm tests are compiled by default test compilation
  compile in MultiJvm <<= (compile in MultiJvm) triggeredBy (compile in Test),
  // disable parallel tests
  parallelExecution in Test := false,
  // make sure that Multi jvm tests are executed by default test target,
  // and combine the results from ordinary test and multi-jvm tests
  executeTests in Test <<= (executeTests in Test, executeTests in MultiJvm) map {
    case (testResults, multiNodeResults) =>
      val overall =
        if (testResults.overall.id < multiNodeResults.overall.id)
          multiNodeResults.overall
        else
          testResults.overall
      Tests.Output(overall, testResults.events ++ multiNodeResults.events,
        testResults.summaries ++ multiNodeResults.summaries)
  }
)

lazy val buildSettings = Defaults.defaultSettings ++ multiJvmSettings ++ Seq(
  organization := "zzz.akka",
  version := "1.0",
  scalaVersion := ScalaVersion,
  autoCompilerPlugins := true
)


val planeDependencies = Seq(
  // ** Center maven.org **
  // Swing Dependency is now in the scala-library-all
  "org.scala-lang" % "scala-library-all" % "2.11.0",
  "com.typesafe.akka" %% "akka-actor"   % AkkaVersion,
  "com.typesafe.akka" %% "akka-remote"  % AkkaVersion,
  "com.typesafe.akka" %% "akka-agent"   % AkkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % AkkaVersion,
  "com.typesafe.akka" %% "akka-multi-node-testkit" % AkkaVersion,
  "org.scalatest"     %% "scalatest"    % ScalaTestVersion,
  "junit" % "junit" % "4.11" % "test"
)


lazy val project = Project(
  id = "airplane-akka",
  base = file("."),
  settings = buildSettings ++ Seq(libraryDependencies ++= planeDependencies)
) configs MultiJvm