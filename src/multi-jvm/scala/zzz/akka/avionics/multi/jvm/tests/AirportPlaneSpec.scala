package zzz.akka.avionics.multi.jvm.tests

import akka.actor.{ActorRef, Actor, Props, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import zzz.akka.avionics.FlyingBehaviour.{Fly, CourseTarget}
import zzz.akka.avionics.{Airport, RemoteConfig}
import scala.concurrent.duration._

class AirportPlaneMultiJvmNode1
  // Construct the ActorSystem using the specialized
  // configuration we create earlier
  extends TestKit(ActorSystem("AirportSpec", RemoteConfig.airportConfig))
with ImplicitSender
with WordSpecLike
with Matchers
with BeforeAndAfterAll {
  // This will give us a purposefully named actor that the plane-side
  // of this test can communicate with
  val receiver = system.actorOf(Props(new Actor {
    def receive: Receive = {
      case m => testActor forward m
    }
  }), "testReceiver")

  override def afterAll() {
    system.shutdown()
  }

  "AirportPlaneSpec" should {
    "start up" in {
      val toronto = system.actorOf(Airport.toronto(), "toronto")
      // We're going to let the other side tell us when to shut down
      expectMsg("stopAirport")
    }
  }
}

class AirportPlaneMultiJvmNode2
  // Construct the Actor system using the specialized
  // configuration we created earlier
  extends TestKit(ActorSystem("PlaneSpec", RemoteConfig.planeConfig))
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  import Airport._
  import RemoteConfig._

  override def afterAll(){
    system.shutdown()
  }

  // Get an ActorRef for the remote system's testReceiver
  def remoteTestReceiver(): ActorRef = system.actorFor(
    "akka.tcp://AirportSpec@" +
      s"$airportHost:$airportPort/user/testReceiver")

  // Get an ActorRef for the remote system's toronto airport
  def toronto(): ActorRef = system.actorFor("akka.tcp://AirportSpec@" +
    s"$airportHost:$airportPort/user/toronto")

  // Tells us whether or not the toronto airport has been found
  def actorForAirport: Boolean = !toronto().isTerminated

  "AirportPlaneSpec" should {
    "get flying instructions from toronto" in {
      // Wait for the toronto airport to come online
      awaitCond(actorForAirport, 3.seconds)
      val to = toronto()
      // Fly there
      to ! DirectFlyerToAirport(testActor)
      // We should be getting Fly directives
      expectMsgPF() {
        case Fly(CourseTarget(altitude, heading, when)) =>
          altitude should be > (1000.0)
          heading should be (314.3f)
          when should be > (0L)
      }
      // We got it. Shut down.
      remoteTestReceiver() ! "stopAirport"
    }
  }

}