package zzz.akka.avionics

import akka.actor._
import zzz.akka.avionics.Plane._
import akka.actor.Terminated
import zzz.akka.avionics.Plane.Controls
import zzz.akka.avionics.Plane.CopilotReference
import zzz.akka.avionics.Pilot.ReadyToGo

object Autopilot {
  def props(plane: ActorRef): Props = Props(new Autopilot(plane))
}
/** Autopilot is a fail-over for copilot.
 * Created by Andriy Drozdyuk on 05-Apr-14.
 */
class Autopilot(plane: ActorRef) extends Actor {
  var copilot = context.system.deadLetters
  var controls = context.system.deadLetters

  // TODO: Implement meaningful copilot
  override def receive: Receive = {
    // Once the plane is ready to go tell the autopilot
    // which will then request a reference to the copilot
    case ReadyToGo =>
      plane ! RequestCopilot
    case CopilotReference(ref: ActorRef) =>
      copilot = ref
      context.watch(copilot)

    case TakeControl =>
      plane ! GiveMeControl
    case Terminated(_) =>
      // Copilot died
      plane ! GiveMeControl
    case Controls(ref) => controls = ref
  }
}
