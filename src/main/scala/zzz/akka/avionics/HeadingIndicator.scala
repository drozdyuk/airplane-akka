package zzz.akka.avionics

import akka.actor.{Props, Actor, ActorLogging}
import scala.concurrent.duration._
import zzz.akka.avionics.StatusReporter.StatusOK

/** Tells us where plane is heading - left, right.
 * Created by Andriy Drozdyuk on 09-Apr-14.
 */
object HeadingIndicator {
  // Indicates that something has changed, how fast
  // we're changing direction
  case class BankChange(amount: Float)

  def props(): Props = Props(new HeadingIndicator with EventSourceImpl)

  // The event published by the HeadingIndicator to listeners
  // that want to know where we're headed
  case class HeadingUpdate(heading: Float)

  // Query about the heading
  case object GetCurrentHeading
  // Query answer
  case class CurrentHeading(heading: Float)
}

trait HeadingIndicator extends Actor
with ActorLogging
with StatusReporter
{
  this: EventSource =>
  import HeadingIndicator._
  import context._

  // Set the status to be always happy!
  def currentStatus = StatusOK

  // Internal message we use to recalculate our heading
  case object Tick

  // Max degrees-per-sec that our plane can move
  val maxDegPerSec = 5

  // Our timer that schedules our updates
  val ticker = system.scheduler.schedule(100.millis, 100.millis,
    self, Tick)

  // Last tick which we can use to calc. our changes
  var lastTick: Long = System.currentTimeMillis()

  // Curr. rate of our bank
  var rateOfBank = 0f

  // Holds our curr. direction
  var heading = 0f

  def headingIndicatorReceive: Receive = {
    // keeps within [-1,1]
    case BankChange(amount) =>
      rateOfBank = amount.min(1.0f).max(-1.0f)

    // Calc. our heading delta based on curr.
    // rate of change, the time delta from our last
    // calc., and the max deg. per sec.
    case Tick =>
      val tick = System.currentTimeMillis()
      val timeDelta = (tick - lastTick) / 1000f
      val degs = rateOfBank * maxDegPerSec
      heading = (heading + (360 + (timeDelta * degs))) % 360
      lastTick = tick
      // Send the HeadingUpdate event to our listeners
      sendEvent(HeadingUpdate(heading))

    case GetCurrentHeading => sender ! CurrentHeading(heading)
  }

  // Remember that we're mixing in the EventSource
  def receive = eventSourceReceive orElse
    headingIndicatorReceive orElse
    statusReceive

  // Don't forget to cancel our timer when we shut down
  override def postStop(): Unit = ticker.cancel()
}
