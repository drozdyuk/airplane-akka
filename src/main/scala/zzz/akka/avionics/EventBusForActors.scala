package zzz.akka.avionics

import akka.event.{LookupClassification, ActorEventBus}

/**
 * Reusable actor event bus.
 * Created by Andriy Drozdyuk on 27-Apr-14.
 */

/**
 * This is a phantom type implementation of a concept
 * that supplies default values to type parameters - something
 * lacking in Scala. Solution is provided by Aaron Novstrup from
 * Stack Overflow:
 * http://stackoverflow.com/a/6629984/230401
 * @tparam A Defaults this type to type B
 * @tparam B Defaults to this type
 */
sealed class DefaultsTo[A,B]
trait LowPriorityDefaultsTo {
  implicit def overrideDefault[A, B] = new DefaultsTo[A, B]
}
object DefaultsTo extends LowPriorityDefaultsTo {
  implicit def default[B] = new DefaultsTo[B, B]
}

object EventBusForActors {
  val classify: Any => Class[_] = { event => event.getClass }
}

class EventBusForActors[EventType, ClassifierType]
(classifier: EventType => ClassifierType = EventBusForActors.classify)
(implicit e: EventType DefaultsTo Any,
          c: ClassifierType DefaultsTo Class[_])
extends ActorEventBus with LookupClassification {

  // Declare that this bus can publish events of any type
  type Event = EventType

  // Classify our events by the class type
  type Classifier = ClassifierType

  protected def classify(event: Event): Classifier = {
    classifier(event)
  }

  protected def mapSize(): Int = 32

  protected def publish(event: Event, subscriber: Subscriber): Unit = {
    subscriber ! event
  }
}
