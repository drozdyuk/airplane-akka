package zzz.akka.avionics

import scala.concurrent.duration.Duration
import akka.actor.{Props, FSM, Actor, ActorRef}
import scala.collection.immutable.Queue
import akka.agent.Agent
import java.util.concurrent.TimeUnit

sealed abstract class Gender
case object Male extends Gender
case object Female extends Gender

/**
 * What is stored in two Agents - one for Male and one for Female
 * passengers.
 * @param gender
 * @param peakDuration
 * @param count
 */
case class GenderAndTime(gender: Gender,
                          peakDuration: Duration,
                          count: Int)

object Bathroom {
  // The states for our FSM
  sealed trait State
  case object Vacant extends State
  case object Occupied extends State

  // The Data for our FSM
  sealed trait Data
  case class InUse(by: ActorRef, atTimeMillis: Long,
                    queue: Queue[ActorRef]) extends Data
  case object NotInUse extends Data

  // Messages to and from the FSM
  case object IWannaUseTheBathroom
  case object YouCanUseTheBathroomNow
  case class Finished(gender: Gender)

  // Helper function to update one of the counters
  private def updateCounter(male: Agent[GenderAndTime],
                             female: Agent[GenderAndTime],
                             gender: Gender, dur: Duration){
    gender match {
      case Male => male send { c =>
        GenderAndTime(Male, dur.max(c.peakDuration), c.count + 1)
      }
      case Female => female send { c =>
        GenderAndTime(Female, dur.max(c.peakDuration), c.count +1)
      }
    }
  }

  def props(femaleBathroomCounter: Agent[GenderAndTime],
            maleBathroomCounter: Agent[GenderAndTime]): Props = Props(new Bathroom(femaleBathroomCounter, maleBathroomCounter))

}

/**
 * Actor responsible for keep track of the state of a particular bathroom,
 * and recording the statistics for their usages.
 * @param femaleCounter Agent to keep track of female bathroom visitors
 * @param maleCounter Agent to keep track of male bathroom visitors
 *  Created by Andriy Drozdyuk on 04-May-14.
 */
class Bathroom(femaleCounter: Agent[GenderAndTime],
                maleCounter: Agent[GenderAndTime]) extends Actor
with FSM[Bathroom.State, Bathroom.Data] {
  import Bathroom._

  startWith(Vacant, NotInUse)

  when(Vacant) {
    case Event(IWannaUseTheBathroom, _) =>
      sender ! YouCanUseTheBathroomNow
      goto(Occupied) using InUse(by = sender,
        atTimeMillis = System.currentTimeMillis(),
        queue = Queue())
  }
  when(Occupied) {
    // Can't use the bathroom now, queue up
    case Event(IWannaUseTheBathroom, data: InUse) =>
      stay using data.copy(queue = data.queue.enqueue(sender))
    // Note that we guard the case by the identity of the sender
    case Event(Finished(gender), data: InUse) if sender == data.by =>
      // Update the appropriate counter
      updateCounter(maleCounter, femaleCounter, gender,
        Duration(System.currentTimeMillis() - data.atTimeMillis, TimeUnit.MILLISECONDS))
      // Move on to the next state
      if (data.queue.isEmpty)
        goto(Vacant) using NotInUse
      else {
        val (next, q) = data.queue.dequeue
        next ! YouCanUseTheBathroomNow
        stay using InUse(next, System.currentTimeMillis(), q)
      }
  }
}


