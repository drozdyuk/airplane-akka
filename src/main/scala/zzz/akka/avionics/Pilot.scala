package zzz.akka.avionics

import akka.actor.{Props, Stash, ActorRef, Actor}
import akka.actor.FSM.{CurrentState, Transition, SubscribeTransitionCallBack}
import zzz.akka.avionics.DrinkingBehaviour.{FeelingLikeZaphod, FeelingTipsy, FeelingSober}
import FlyingBehaviour._
import akka.util.Timeout
import scala.concurrent.duration._


/**
 * Pilot of the plane.
 * Created by Andriy Drozdyuk on 05-Apr-14.
 */
object Pilot {
  def props(plane: ActorRef, autopilot: ActorRef, heading: ActorRef, altimeter: ActorRef): Props =
    Props(new Pilot(plane, autopilot, heading, altimeter) with DrinkingProvider with FlyingProvider)

  import FlyingBehaviour._
  import ControlSurfaces._

  /** Receive a copilot */
  case class CopilotAssigned(copilot: ActorRef)

  case object ReadyToGo

  case object RelinquishControl

  // Calculates elevator changes when we're a bit tipsy
  val tipsyCalcElevator: Calculator = {
    (target, status) =>
      val msg = calcElevator(target, status)
      msg match {
        case StickForward(amt) => StickForward(amt * 1.03f)
        case StickBack(amt) => StickBack(amt * 1.03f)
        case m => m
      }
  }

  // Calculates the aileron changes when we're a bit tipsy
  val tipsyCalcAilerons: Calculator = {
    (target, status) =>
      val msg = calcAilerons(target, status)
      msg match {
        case StickLeft(amt) => StickLeft(amt * 1.03f)
        case StickRight(amt) => StickRight(amt * 1.03f)
        case m => m
      }
  }

  // Calculates elevator changes when pilot is out of it
  val zaphodCalcElevator: Calculator = {
    (target, status) =>
      val msg = calcElevator(target, status)
      msg match {
        case StickForward(_) => StickBack(1f)
        case StickBack(_) => StickForward(1f)
        case m => m
      }
  }

  // Calculates aileron changes when we're out of it
  val zaphodCalcAilerons: Calculator = {
    (target, status) =>
      val msg = calcAilerons(target, status)
      msg match {
        case StickLeft(_) => StickRight(1f)
        case StickRight(_) => StickLeft(1f)
        case m => m

      }
  }
}
class Pilot(plane: ActorRef,
            autopilot: ActorRef,
            heading: ActorRef,
            altimeter: ActorRef) extends Actor with Stash{
  this: DrinkingProvider with FlyingProvider =>
  import Pilot._

  // Implicit execution context for futures
  implicit val ec = context.dispatcher
  // Implicit timeout for getting dependencies
  implicit val timeout = Timeout(1.second)

  // Create our children
  val drinker = context.actorOf(newDrinkingBehaviour(self), "DrinkingBehaviour")
  val flyer = context.actorOf(newFlyingBehaviour(plane, heading, altimeter), "FlyingBehaviour")
  
  // Copilot is not vital for pilot's operation
  var coPilot = context.system.deadLetters

  def receive: Receive = {
    case CopilotAssigned(ref) =>
      coPilot = ref
      
    case ReadyToGo =>
      flyer ! SubscribeTransitionCallBack(self)
      setCourse(flyer)
      context.become(sober)
  }


  def setCourse(flyer: ActorRef) {
    flyer ! Fly(CourseTarget(20000, 250, System.currentTimeMillis() + 30000))
  }

  
  // Sober behaviour
  def sober: Receive = {
    case FeelingSober => // We're already sober
    case FeelingTipsy => becomeTipsy
    case FeelingLikeZaphod => becomeZaphod
  }

  // Tipsy behaviour
  def tipsy: Receive = {
    case FeelingSober => becomeSober
    case FeelingTipsy => // already tipsy
    case FeelingLikeZaphod => becomeZaphod
  }

  // Zaphod behaviour
  def zaphod: Receive = {
    case FeelingSober => becomeSober
    case FeelingTipsy => becomeTipsy
    case FeelingLikeZaphod => // already zaphod
  }

  // Idle state is merely the one where Pilot does nothing at all
  def idle: Receive = {
    case _ =>
  }
  // Updates the flying behaviour with sober calcs
  // and then becomes the sober behaviour
  def becomeSober() = {
    flyer ! NewElevatorCalculator(calcElevator)
    flyer ! NewBankCalculator(calcAilerons)
    context.become(sober)
  }

  // Updates flying behaviour with tipsy calculation
  // then becomes tipsy
  def becomeTipsy() = {
    flyer ! NewElevatorCalculator(tipsyCalcElevator)
    flyer ! NewBankCalculator(tipsyCalcAilerons)
    context.become(tipsy)
  }

  def becomeZaphod() = {
    flyer ! NewElevatorCalculator(zaphodCalcElevator)
    flyer ! NewBankCalculator(zaphodCalcAilerons)
    context.become(zaphod)
  }

  // At any time the FlyingBehaviour could go back to an idle state
  // which means that our behaviour changes don't matter any more
  override def unhandled(msg: Any): Unit = {
    msg match {
      case Transition(_, _, Flying) => setCourse(sender)
      case Transition(_, _, Idle) => context.become(idle)
      // Ignore these two messages from the FSM rather than have
      // them go to the log
      case Transition(_, _, _) =>
      case CurrentState(_, _) =>
      case m => super.unhandled(m)
    }
  }
}