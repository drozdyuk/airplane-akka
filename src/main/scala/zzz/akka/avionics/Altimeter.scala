package zzz.akka.avionics

import akka.actor.{Props, ActorLogging, Actor}
import scala.concurrent.duration._
import zzz.akka.avionics.StatusReporter.{StatusOK, Status}

class Altimeter extends Actor
with ActorLogging
with StatusReporter
{
  // Self type - injection of event source
  this: EventSource =>

  import Altimeter._
  // we need execution context for the scheduler
  // use actor's own dispatcher to schedule work
  implicit val ec = context.dispatcher

  // the max ceiling of our plane in 'feet'
  val ceiling = 43000

  // max rate of climb for our plane in 'feet per minute'
  val maxRateOfClimb = 5000

  // varying rate of climb dep on the movement of the stick
  var rateOfClimb = 0f

  // our curr altitude
  var altitude = 0d

  // as time passes, we need to change the altitude based on the time passed.
  // The lastTick allows us to figure out how much time has passed
  var lastTick = System.currentTimeMillis()

  // we need to periodically update our altitude. This schedule message send
  // will tell us when to do that
  val ticker = context.system.scheduler.schedule(100.millis, 100.millis, self, Tick)

  // Our internal message we send to ourselves to tell us to update our altitude
  case object Tick

  def receive = statusReceive orElse eventSourceReceive orElse {
    // our rate of climb has changed
    case RateChange(amount) =>
      // Truncate the rate of 'amount' to [-1,1] before multiplying
      rateOfClimb = amount.min(1.0f).max(-1.0f) * maxRateOfClimb
      log info s"Altimeter changed rate of climb to $rateOfClimb"

    // calculate new altitude
    case Tick =>
      val tick = System.currentTimeMillis()
      altitude = altitude + ((tick - lastTick) / 60000.0) * rateOfClimb
      lastTick = tick
      sendEvent(AltitudeUpdate(altitude))

    // Someone wants to know the current altitude
    case GetCurrentAltitude =>
      sender ! CurrentAltitude(altitude)

  }

  // kill our ticker when we stop
  override def postStop(): Unit = ticker.cancel()

  /**
   * Current status
   * @return the current status of the altimeter.
   */
  override def currentStatus: Status = StatusOK
}
object Altimeter {
  case class RateChange(amount: Float)
  // Sent by altimeter at regular intervals
  case class AltitudeUpdate(altitude: Double)

  // Query altimeter about altitude
  case object GetCurrentAltitude
  // Respond to altitude query
  case class CurrentAltitude(altitude: Double)


  /**
   * Recommended practice for akka actors. Define a props method
   * on a companion object:
   * http://doc.akka.io/docs/akka/2.3.2/scala/actors.html
   * @return Configuration object for creating an altimeter.
   */
  def props(): Props = Props(new Altimeter with EventSourceImpl)

}
