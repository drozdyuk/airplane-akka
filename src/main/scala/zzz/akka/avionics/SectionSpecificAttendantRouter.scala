/** Andriy: I have no idea how to re-implement this in current akka version */
/*
package zzz.akka.avionics

import akka.routing._
import akka.actor.{ActorRef, Props, SupervisorStrategy, ActorSystem}
import akka.dispatch.Dispatchers
import akka.routing.Router
import scala.collection.immutable.{Iterable, IndexedSeq}

/** Custom router to make attendants handle requests based on row or seat
  * numbers.
 * Created by drozdyuka on 16-Apr-14.
 */

class SpecificRoutingLogic extends RoutingLogic {
  override def select(message: Any, routees: IndexedSeq[Routee]): Routee = {
    //??? /no /sender /here!


  }
}

class SectionSpecificAttendantRouter extends RouterConfig {
  this: FlightAttendantProvider =>

  override def routerDispatcher: String = Dispatchers.DefaultDispatcherId

  //override def supervisorStrategy: SupervisorStrategy = SupervisorStrategy.defaultStrategy

  override def createRouter(system: ActorSystem): Router = {
    // Create 5 flight attendants
    val attendants = (1 to 5) map { n =>
      system.actorOf(Props(newFlightAttendant()), "Attendant-" + n)

    }

    new Router(new SpecificRoutingLogic())
  }

  override private[akka] def createRouterActor(): ActorRef = {
    ???
  }
}
*/
