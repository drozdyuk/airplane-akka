package zzz.akka.avionics

import scala.concurrent.duration._
import akka.util.Timeout
import akka.actor.{Props, ActorSystem}


object Avionics {
  // needed for '?' below
  implicit val timeout = Timeout(5.seconds)
  val system = ActorSystem("PlaneSimulation")


  def main(args: Array[String]) {
    implicit val ec = system.dispatcher
    val plane = system.actorOf(Plane.props(), "Plane")

    // Wait for the plane to start
    val server = system.actorOf(TelnetServer.props(plane, 31777), "Telnet")


    // Grab the controls
    /*val control = Await.result(
      (plane ? Plane.GiveMeControl).mapTo[ActorRef],
      5.seconds
    )
    // Takeoff
    system.scheduler.scheduleOnce(200.millis) {
      control ! ControlSurfaces.StickBack(1f)
    }

    // Level out
    system.scheduler.scheduleOnce(1.seconds) {
      control ! ControlSurfaces.StickBack(0f)
    }

    // Climb
    system.scheduler.scheduleOnce(3.seconds) {
      control ! ControlSurfaces.StickBack(0.5f)
    }

    // Level out
    system.scheduler.scheduleOnce(4.seconds) {
      control ! ControlSurfaces.StickBack(0f)
    }

    // Shut down
    system.scheduler.scheduleOnce(5.seconds) {
      system.shutdown()
    }*/

  }
}