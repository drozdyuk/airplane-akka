package zzz.akka.avionics

import scala.concurrent.duration._
import scala.util.Random
import akka.actor.{Props, ActorLogging, Actor, ActorRef}
import zzz.akka.avionics.FlightAttendant.{Drink, GetDrink}

/** Simulate routing to passengers via broadcast.
 * Created by Andriy Drozdyuk on 14-Apr-14.
 */
object Passenger {
  // These are notifications to tell Passenger to fasten
  // or unfasten their seat belts
  case object FastenSeatbelts
  case object UnfastenSeatbelts

  def props(callButton: ActorRef, bathrooms: ActorRef): Props = {
    Props(new Passenger(callButton, bathrooms) with DrinkRequestProbability)
  }

  // Reg-exp to extract Name-Row-Seat tuple
  val SeatAssignment = """([\w\s_]+)-(\d+)-([A-Z])""".r

}

// Defines some thresholds that we can modify in tests
// to speed things up.
trait DrinkRequestProbability {
  // Limits the decision on whether the passenger actually
  // asks for a drink
  val askThreshold = 0.9f

  // The minimum time between drink requests
  val requestMin = 20.minutes

  // Some portion of this (0 to 100 percent) is added to requestMin
  val requestUpper = 30.minutes

  // Gives us a 'random' time within the previous two bounds
  def randomishTime(): FiniteDuration = {
    requestMin + Random.nextInt(requestUpper.toMillis.toInt).millis
  }

}

// Idea behind this trait is to use it in other classes to
// gives us the ability to slide in different Actor types
// to ease the testing
trait PassengerProvider {
  def newPassenger(callButton: ActorRef, bathrooms: ActorRef): Props = Passenger.props(callButton, bathrooms)
}

/**
 * Passenger actor.
 *
 * @param callButton Call button for a flight attendant
 *                   to whom this passenger can send a
 *                   GetDrink message.
 * @param bathrooms Reference to the bathrooms on the plane. Send it IWannaUseTheBathroomNow message,
 *                  and when received YouCanUseTheBathroom now message - record the sender as the
 *                  bathroom to use. Afterwards, send it the Finished with our gender message.
 */
class Passenger(callButton: ActorRef, bathrooms: ActorRef) extends Actor with ActorLogging {
  this: DrinkRequestProbability =>
  import Passenger._
  import scala.collection.JavaConverters._

  implicit val ec = context.dispatcher

  val r = scala.util.Random

  // Self message indicating it's time to ask for a drink!
  case object CallForDrink

  // Name of passenger can't have spaces in it,
  // since that's not a valid char in the URI spec. We know
  // the name will have underscores in place of spaces,
  // and we'll convert those back - here.
  val SeatAssignment(myName, _, _)  =
    self.path.name.replaceAllLiterally("_", " ")

  // We'll be pulling some drink names from the config.
  val drinks = context.system.settings.config.getStringList(
  "zzz.akka.avionics.drinks").asScala.toIndexedSeq

  // Shortcut for scheduler
  val scheduler = context.system.scheduler

  // We've just sat down, so it's time to get a drink
  override def preStart() {
    self ! CallForDrink
  }

  // This method will decide whether or not we actually want
  // to get a drink using some randomness
  def maybeSendDrinkRequest(): Unit = {
    if (r.nextFloat() > askThreshold) {
      val drinkName = drinks(r.nextInt(drinks.length))
      callButton ! GetDrink(drinkName)
    }

    scheduler.scheduleOnce(randomishTime(), self, CallForDrink)
  }

  def receive = {
    case CallForDrink =>
      maybeSendDrinkRequest()
    case Drink(drinkName) =>
      log.info(s"$myName received $drinkName - Yum!")
    case FastenSeatbelts =>
      log.info(s"$myName fastening seatbelt.")
    case UnfastenSeatbelts =>
      log.info(s"$myName unfastening seatbelt")
  }







}

