package zzz.akka.avionics

import akka.actor.Actor

object StatusReporter {
  /** The message indicating that status should be reported  */
  case object ReportStatus

  // The different types of status that can be reported
  sealed trait Status
  case object StatusOK extends Status
  case object StatusNotGreat extends Status
  case object StatusBAD extends Status
}

/** Mix in that will allow any instrument to report on it's status.
  * Anyone can simply send this entity a ReportStatus request,
  * and will receive a Status response back.
 * Created by Andriy Drozdyuk on 19-Apr-14.
 */
trait StatusReporter { this: Actor =>
  import StatusReporter._

  /**
   * Implementers need to define this.
   * @return the current status of the object.
   */
  def currentStatus: Status

  /**
   * Must be combined with orElse into the ultimate receive method
   * @return Receive partial function
   */
  def statusReceive: Receive = {
    case ReportStatus =>
      sender ! currentStatus
  }
}


