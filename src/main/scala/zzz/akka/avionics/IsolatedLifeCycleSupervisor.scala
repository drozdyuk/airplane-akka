package zzz.akka.avionics

import akka.actor.{ActorKilledException, ActorInitializationException, Actor}
import scala.concurrent.duration.Duration
import akka.actor.SupervisorStrategy.{Escalate, Resume, Stop}

/** Provides supervisor's base functionality that lets children survive
  * the supervisor's own restarts, as well as provides some extra plumbing
  * to make that happen.
 * Created by Andriy on 05-Apr-14.
 */

object IsolatedLifeCycleSupervisor {
  // Message we use in case we want people to be
  // able to wait for us to finish starting
  case object WaitForStart
  case object Started
}

trait IsolatedLifeCycleSupervisor extends Actor{
  import IsolatedLifeCycleSupervisor._
  def receive = {
    // Signify we've started
    case WaitForStart =>
      sender ! Started
    // We don't handle anything else, only error
    case m => throw new Exception(s"Don't call ${self.path.name} directly ($m).")
  }

  // To be implemented by subclass
  def childStarter(): Unit

  // Only start the children when we're started
  final override def preStart() { childStarter() }

  // Don't call preStart(), which would be default behavior
  final override def postRestart(reason: Throwable) {}

  // Don't stop the children, which would be the default behavior
  final override def preRestart(reason:Throwable, message:Option[Any]){}
}

abstract class IsolatedResumeSupervisor(
    maxNrRetries: Int = -1,
    withinTimeRange: Duration = Duration.Inf)
  extends IsolatedLifeCycleSupervisor{
  this: SupervisionStrategyFactory =>

  override val supervisorStrategy = makeStrategy(maxNrRetries, withinTimeRange){
    case _: ActorInitializationException => Stop
    case _: ActorKilledException => Stop
    case _: Exception => Resume
    case _ => Escalate
  }
}

abstract class IsolatedStopSupervisor(
    maxNrRetries: Int = -1,
    withinTimeRange: Duration = Duration.Inf)
  extends IsolatedLifeCycleSupervisor {
  this: SupervisionStrategyFactory =>

  override val supervisorStrategy = makeStrategy(maxNrRetries, withinTimeRange){
    case _: ActorInitializationException => Stop
    case _: ActorKilledException => Stop
    case _: Exception => Stop
    case _ => Escalate
  }
}
