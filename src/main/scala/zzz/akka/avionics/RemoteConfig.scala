package zzz.akka.avionics

import com.typesafe.config.ConfigFactory

/** Brings in the plane-remote and airport-remote from the config
  * Created by Andriy Drozdyuk on 30-Apr-14.
 */
object RemoteConfig {
  // Get the configuration object for the plane
  val planeConfig = ConfigFactory.load().getConfig("plane-remote")
  // Easy access to the host of the airport
  val airportHost = planeConfig.getString("zzz.akka.avionics.airport-host")
  // Easy access to the port of the airport
  val airportPort = planeConfig.getString("zzz.akka.avionics.airport-port")

  // Get the configuration object for the airport
  val airportConfig = ConfigFactory.load().getConfig("airport-remote")
  val planeHost = airportConfig.getString("zzz.akka.avionics.plane-host")
  val planePort = airportConfig.getString("zzz.akka.avionics.plane-port")
}
