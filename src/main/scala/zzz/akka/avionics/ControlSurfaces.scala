package zzz.akka.avionics

import akka.actor.{Props, Actor, ActorRef}
import zzz.akka.avionics.HeadingIndicator.BankChange

// The Control Surfaces object carries messages
// for controlling the plane
object ControlSurfaces {
  def props(plane: ActorRef, altimeter: ActorRef, headingIndicator: ActorRef): Props =
    Props(new ControlSurfaces(plane, altimeter, headingIndicator))


  // amount is value between -1 and 1.
  // Excessive values will be truncated.
  case class StickForward(amount: Float)
  case class StickBack(amount:Float)
  case class StickLeft(amount:Float)
  case class StickRight(amount:Float)
  case class HasControl(somePilot: ActorRef)
}

// Pass in the Altimeter as an ActorRef
// so that we can send messages to it
class ControlSurfaces(plane: ActorRef, altimeter: ActorRef, headingIndicator: ActorRef) extends Actor {
  import ControlSurfaces._
  import Altimeter._

  // Instantiate the receive method by saying that the
  // ControlSurfaces are controlled by the dead letter office.
  // Effectively, this says that nothing's currently in control.
  def receive = controlledBy(context.system.deadLetters)

  // As control is transferred betw. diff. entities,
  // we will change the instantiated receive func. with
  // new variants. This closure ensures that only the assigned
  // pilot can control the plane.
  def controlledBy(somePilot: ActorRef): Receive = {
    // Pilot pulled back on the stick a certain
    // amount and we inform altimeter we're climbing
    case StickBack(amount) if sender == somePilot => altimeter ! RateChange(amount)
    case StickForward(amount) if sender == somePilot => altimeter ! RateChange(-1 * amount)
    case StickLeft(amount) if sender == somePilot => headingIndicator ! BankChange(-1 * amount)
    case StickRight(amount) if sender == somePilot => headingIndicator ! BankChange(amount)
    // Only the plane can tell us who's currently in control
    case HasControl(entity) if sender == plane =>
      // Become a new instance, where entity, which the plane told us about,
      // is now the entity that controls the plane
      context.become(controlledBy(entity))

  }
}
