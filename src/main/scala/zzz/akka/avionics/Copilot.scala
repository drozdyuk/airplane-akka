package zzz.akka.avionics

import akka.actor._
import zzz.akka.avionics.Plane.GiveMeControl
import akka.actor.Terminated
import zzz.akka.avionics.Plane.Controls
import akka.util.Timeout
import scala.concurrent.duration._
import zzz.akka.avionics.Copilot.PilotAssigned

object Copilot {
  /** Receive a pilot */
  case class PilotAssigned(pilot: ActorRef)

  def props(plane: ActorRef, autopilot: ActorRef, altimeter: ActorRef): Props =
    Props(new Copilot(plane, autopilot, altimeter))
}
/** Copilot is a fail-over for the pilot.
 * Created by Andriy Drozdyuk on 05-Apr-14.
 */
class Copilot(plane: ActorRef, autopilot: ActorRef, altimeter: ActorRef) extends Actor with Stash {
  import Pilot._
  // Implicit execution context for futures
  implicit val ec = context.dispatcher
  // Implicit timeout for getting dependencies
  implicit val timeout = Timeout(1.second)

  val conf = context.system.settings.config
  var controls: ActorRef = context.system.deadLetters

  def receive: Receive = waitingForPilot

  def waitingForPilot: Receive = {
    case PilotAssigned(pilot) =>
      // Get all the messages we stashed and receive them
      unstashAll()
      // pass all our acquired dependencies in
      context.become(operational(pilot))

    // Any other message save for later
    case _ => stash()
  }

  // All our dependencies have been acquired
  def operational(pilot: ActorRef) : Receive = {
    case ReadyToGo =>
      // Start watch on the pilot in case he kicks it
      context.watch(pilot)

    case Controls(ctr) =>
      controls = ctr

    case Terminated(_) =>
      // Pilot died!
      plane ! GiveMeControl

  }


}
