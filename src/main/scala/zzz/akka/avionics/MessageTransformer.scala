package zzz.akka.avionics

import akka.actor.{Actor, ActorRef}

/**
 * Created by Andriy Drozdyuk on 28-Apr-14.
 */
class MessageTransformer(from: ActorRef, to: ActorRef,
                          transformer: PartialFunction[Any, Any]) extends Actor {
  import EventSource._

  // Automatically register our-self for messages 'from'
  override def preStart() {
    from ! RegisterListener(self)
  }

  // Automatically de-register our-self for message 'from'
  override def postStop() {
    from ! UnregisterListener(self)
  }

  // Take incoming, transform and send outgoing.
  // Note how we keep the original sender as 'from',
  // hiding the transformer from the ultimate receiver
  def receive = {
    case m => to forward transformer(m)
  }

}
