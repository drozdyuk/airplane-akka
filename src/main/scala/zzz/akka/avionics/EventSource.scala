package zzz.akka.avionics

import akka.actor.{Actor, ActorRef}
import akka.actor.Actor.Receive

/** Represents an actor that can act as an event source.
  * Actor gains the RegisterListener/UnregisterListener capabilities, and
  * a method sendEvent which will send an event to all currently registered listeners.
  *
  * Actor must also incorporate eventSourceReceive into its receive function.
 * Created by Andriy Drozdyuk on 4/3/14.
 */
object EventSource {
  // messages used by listeners to register and unregister themselves
  case class RegisterListener(listener: ActorRef)
  case class UnregisterListener(listener: ActorRef)
}

trait EventSource {
  def sendEvent[T](event: T): Unit
  def eventSourceReceive: Receive
}

trait EventSourceImpl extends EventSource { this: Actor =>
  import EventSource._
  // We're going to use a vector but many structures would be adequate
  var listeners = Vector.empty[ActorRef]

  // sends the event to all of our listeners
  def sendEvent[T](event:T):Unit = listeners foreach {
    _ ! event
  }

  // we create a specific partial func to handle messages
  // for our event listener. Anything that mixes in our
  // trait will need to compose this receiver
  def eventSourceReceive: Receive = {
    case RegisterListener(listener) =>
      listeners = listeners :+ listener
    case UnregisterListener(listener) =>
      listeners = listeners filter {_ != listener}
  }
}
