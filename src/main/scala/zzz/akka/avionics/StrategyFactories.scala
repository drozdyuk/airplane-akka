package zzz.akka.avionics

import scala.concurrent.duration.Duration
import akka.actor.SupervisorStrategy.Decider
import akka.actor.{AllForOneStrategy, OneForOneStrategy, SupervisorStrategy}

/** SupervisionStrategyFactory trait (for self-typing by other traits)
  * and helper factories for creating OneForOne and AllForOne strategies.
  *
 * Created by Andriy Drozdyuk on 05-Apr-14.
 */
trait SupervisionStrategyFactory {
  def makeStrategy(maxNrRetries: Int, withinTimeRange: Duration)(decider: Decider): SupervisorStrategy
}

trait OneForOneStrategyFactory extends  SupervisionStrategyFactory {
  def makeStrategy(maxNrRetries: Int, withinTimeRange: Duration)(decider: Decider): SupervisorStrategy = {
    OneForOneStrategy(maxNrRetries, withinTimeRange)(decider)
  }
}

trait AllForOneStrategyFactor extends SupervisionStrategyFactory {
  def makeStrategy(maxNrRetries: Int, withinTimeRange: Duration)(decider: Decider): SupervisorStrategy = {
    AllForOneStrategy(maxNrRetries, withinTimeRange)(decider)
  }
}