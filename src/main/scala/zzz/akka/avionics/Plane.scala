package zzz.akka.avionics

import akka.actor._
import scala.concurrent.{Future, Await}
import zzz.akka.avionics.IsolatedLifeCycleSupervisor.WaitForStart
import scala.concurrent.duration._
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import zzz.akka.avionics.StatusReporter._
import akka.routing.FromConfig
import zzz.akka.avionics.Pilot.ReadyToGo
import akka.agent.Agent
import akka.actor.SupervisorStrategy.Resume
import akka.routing.RoundRobinPool
import zzz.akka.avionics.Pilot.CopilotAssigned
import zzz.akka.avionics.EventSource.RegisterListener
import zzz.akka.avionics.StatusReporter.Status
import zzz.akka.avionics.Copilot.PilotAssigned

object Plane {

  /**
   * The current status of all instruments. Only returns
   * StatusOK if all the instruments are functioning
   * properly.
   */
  case object GetAllInstrumentsStatus

  // Returns the control surface to the Actor that asks for them
  case object GiveMeControl


  // Response to the GiveMeControl message
  case class Controls(controls: ActorRef)

  // Request a reference to copilot
  case object RequestCopilot

  case class CopilotReference(copilot: ActorRef)

  // When something looses control of the plane, it will
  // send this message to the plane
  case object LostControl

  // Tell a pilot-type object to take control of the plane.
  case object TakeControl

  def props(): Props = {
    Props(new Plane with AltimeterProvider
      with HeadingIndicatorProvider
      with PilotProvider
      with FlightAttendantProvider)
  }

}

// We want the Plane to own the altimeter and we're going to
// do that by passing in a specific factory we can use to
// build the altimeter
class Plane extends Actor with ActorLogging {
  this: AltimeterProvider
    with PilotProvider
    with HeadingIndicatorProvider
    with FlightAttendantProvider =>

  import Altimeter._
  import Plane._
  import HeadingIndicator._

  val configField = "zzz.akka.avionics.flightCrew"
  val config = context.system.settings.config
  val pilotName = config.getString(s"$configField.pilotName")
  val copilotName = config.getString(s"$configField.copilotName")
  val attendantName = config.getString(s"$configField.leadAttendantName")

  // Need timeout for asks
  implicit val askTimeout = Timeout(1.second)

  // Execution context
  implicit val ec = context.dispatcher


  override def preStart() {
    // Get our children going. Order is important.
    startEquipment()
    startUtilities()
    startPeople()

    // Bootstrap the system
    actorForControls("Altimeter") ! RegisterListener(self)
    actorForControls("HeadingIndicator") ! RegisterListener(self)
    // TODO: Send a reference to pilot/copilot here? Instead of actorFor? e.g. ReadyToGo(pilot)
    // But where to get ref to pilot?
    actorForPilots(pilotName) ! ReadyToGo
    actorForPilots(copilotName) ! ReadyToGo

    // Let autopilot system know everything is a go
    actorForControls("Autopilot") ! ReadyToGo

  }

  override def postStop() {
    // Await the values. If the plane is shutting down, then,
    // most likely, the whole system is going down. If we
    // get a Future result to the values then we may not get them
    // until the system has disappeared. Bad news.
    val male: Agent[GenderAndTime] = Await.result(maleBathroomCounter.future, 3.seconds).asInstanceOf[Agent[GenderAndTime]]
    val female = Await.result(femaleBathroomCounter.future, 3.seconds).asInstanceOf[Agent[GenderAndTime]]

    log.info(s"${male.get.count} men used the bathroom")
    log.info(s"${female.get.count} women used the bathroom")
    log.info(s"peak bathroom usage time: men = ${male.get.peakDuration}, women = ${female.get.peakDuration}")
  }

  def receive = {
    case AltitudeUpdate(altitude) =>
    //log info s"Altitude is now: $altitude"
    case HeadingUpdate(heading) =>
    //log info s"Heading is now: $heading"
    case RequestCopilot =>
    //sender ! CopilotReference(copilot)
    case GiveMeControl =>
      log info "Plane giving control."
      //TODO: This was like this before, but controls is not longer accessible:
      //      sender ! Controls(controls)
      sender ! Controls(actorForControls("ControlSurfaces"))
    case GetCurrentAltitude =>
      val altimeter = actorForControls("Altimeter")
      altimeter forward GetCurrentAltitude

    case GetCurrentHeading =>
      val headingIndicator = actorForControls("HeadingIndicator")
      headingIndicator forward GetCurrentHeading

    case LostControl =>
      actorForControls("Autopilot") ! TakeControl
    case GetAllInstrumentsStatus =>
      val headingIndicator = actorForControls("HeadingIndicator")
      val altimeter = actorForControls("Altimeter")

      val instruments = Vector(altimeter, headingIndicator)

      Future.sequence(instruments.map {
        instrument =>
          (instrument ? ReportStatus).mapTo[Status]
      }) map {
        results =>
          if (results.contains(StatusBAD)) StatusBAD
          else if (results.contains(StatusNotGreat)) StatusNotGreat
          else StatusOK
      } pipeTo sender
  }

  def startEquipment() {
    val plane = self

    val controls = context.actorOf(Props(new IsolatedResumeSupervisor with OneForOneStrategyFactory {
      def childStarter() {
        val alt = context.actorOf(newAltimeter, "Altimeter")
        val headingInd = context.actorOf(newHeadingIndicator, "HeadingIndicator")
        // These children get implicitly added to the hierarchy
        context.actorOf(newAutopilot(plane), "Autopilot")
        context.actorOf(ControlSurfaces.props(plane, alt, headingInd), "ControlSurfaces")
      }
    }), "Equipment")
    Await.result(controls ? WaitForStart, 1.second)
  }

  val maleBathroomCounter = Agent(GenderAndTime(Male, 0.seconds, 0))
  val femaleBathroomCounter = Agent(GenderAndTime(Female, 0.seconds, 0))

  // Start 4 bathrooms behind a 4-instance router
  def startUtilities() {
    context.actorOf(
      RoundRobinPool(nrOfInstances = 4, supervisorStrategy = OneForOneStrategy() {
        case _ => Resume
      }).props(Bathroom.props(femaleBathroomCounter, maleBathroomCounter)), "Bathrooms")
  }

  def startPeople() {
    val plane = self

    val controls: ActorRef = actorForControls("ControlSurfaces")
    val autopilot: ActorRef = actorForControls("Autopilot")
    val altimeter: ActorRef = actorForControls("Altimeter")
    val bathrooms = context.actorFor("Bathrooms")

    // Use the Router as defined in the  configuration file
    // under the name "/Plane/FlightAttendantRouter"
    //    val router = context.actorOf(BroadcastGroup(paths).props(), "Passengers")

    val leadAttendant = context.actorOf(FromConfig.props(newFlightAttendant()),
      "FlightAttendantRouter")

    val people = context.actorOf(Props(new IsolatedStopSupervisor
      with OneForOneStrategyFactory {
      override def childStarter() {
        context.actorOf(PassengerSupervisor.props(leadAttendant, bathrooms))

        // These children get implicitly added to the hierarchy
        val copilot = context.actorOf(newCopilot(plane, autopilot, altimeter), copilotName)
        val pilot = context.actorOf(newPilot(plane, autopilot, controls, altimeter), pilotName)
        copilot ! PilotAssigned(pilot)
        pilot ! CopilotAssigned(copilot)
      }
    }), "Pilots")

    Await.result(people ? WaitForStart, 1.second)
  }

  // Helps us look up Actors within the "Pilots" Supervisor
  def actorForPilots(name: String) = context.actorFor("Pilots/" + name)

  // Helps us look up Actors within the "Equipment" Supervisor
  def actorForControls(name: String): ActorRef = {
    context.actorFor("Equipment/" + name)
  }


}
