package zzz.akka.avionics

import akka.actor.{ActorSystem, ActorRef, Props, Actor}
import scala.util.Random
import zzz.akka.avionics.LeadFlightAttendant.{Attendant, GetFlightAttendant}

/** LeadFlightAttendant automatically takes care of creating child flight attendants,
  * and forwarding them the tasks.
 * Created by Andriy Drozdyuk on 05-Apr-14.
 */
// Lead is going to construct its own subordinates
// We'll have a policy to vary that
trait AttendantCreationPolicy {
  // Feel free to make this configurable!
  val numberOfAttendants: Int = 8
  def createAttendant: Props = FlightAttendant.props()
}

object LeadFlightAttendant {
  case object GetFlightAttendant
  case class Attendant(a: ActorRef)

  def props(): Props = Props(new LeadFlightAttendant with AttendantCreationPolicy)

}
class LeadFlightAttendant extends Actor {
  this: AttendantCreationPolicy =>
  // After getting lead, we're going to have it create all
  // its subordinates
  override def preStart() {
    import scala.collection.JavaConverters._

    val attendantNames = context.system.settings.config.getStringList(
      "zzz.akka.avionics.flightCrew.attendantNames").asScala

    attendantNames take numberOfAttendants foreach {
      // we create the actors within our context s.t. they are
      // children of this actor
      name => context.actorOf(createAttendant, name)
    }
  }
  def randomAttendant(): ActorRef = {
    context.children.take(Random.nextInt(numberOfAttendants) + 1).last
  }
  def receive = {
    case GetFlightAttendant =>
      sender ! Attendant(randomAttendant())
    case m => randomAttendant() forward m
  }

}


object FlightAttendantChecker {
  def main(args: Array[String]){
    val system = ActorSystem("PlaneSimulation")
    val lead = system.actorOf(Props(new LeadFlightAttendant with AttendantCreationPolicy),
      system.settings.config.getString("zzz.akka.avionics.flightCrew.leadAttendantName"))
    Thread.sleep(2000)
    system.shutdown()
  }
}

