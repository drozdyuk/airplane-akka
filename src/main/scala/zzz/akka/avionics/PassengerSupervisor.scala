package zzz.akka.avionics

import akka.actor._
import akka.actor.SupervisorStrategy.{Stop, Resume, Escalate}
import akka.actor.OneForOneStrategy
import akka.actor.ActorKilledException
import com.typesafe.config.ConfigList
import akka.routing.{BroadcastGroup, BroadcastRouter}
import scala.collection.immutable.Iterable
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import scala.concurrent.duration._

object PassengerSupervisor {
  /** Allows someone to request the Broadcast Router */
  case object GetPassengerBroadcaster

  /** Returns the BroadcastRouter to the requester */
  case class PassengerBroadcaster(broadcaster: ActorRef)

  // Factory method for easy construction
  def props(callButton: ActorRef, bathrooms: ActorRef): Props =
    Props(new PassengerSupervisor(callButton, bathrooms: ActorRef) with PassengerProvider)

}

/** Passengers supervisor that contains a broadcast capability
 * to send a message to all the passengers. To request the broadcaster
 * issue a GetPassengerBroadcaster message. Response of type
 * PassengerBroadcaster(router) will be sent to the requester.
 * Created by Andriy Drozdyuk on 15-Apr-14.
 *
 * @param callButton Actor (like flight attendant ) that can serve each passengers needs,
  *                   for example, getting a drink
 */
class PassengerSupervisor(callButton: ActorRef, bathrooms: ActorRef) extends Actor {
  this: PassengerProvider =>
  import PassengerSupervisor._

  implicit val ec = context.dispatcher

  // We'll resume our immediate children instead of restarting them
  // on an Exception
  override val supervisorStrategy = OneForOneStrategy(){
    case _:ActorKilledException => Escalate
    case _:ActorInitializationException => Escalate
    case _ => Resume
  }

  // Internal messages we use to communicate between this Actor
  // and its subordinate supervisor
  case class GetChildren(forSomeone: ActorRef)

  // We use pre-start to create our supervisor
  override def preStart() {

    /* The real supervisor - it creates all the necessary
     * passengers under the stop supervision strategy,
      * and allows anyone to request them via the
      * GetChildren(...) message - to which it sends
      * a Children(..) response.
     */
    context.actorOf(Props(new Actor {
      // shortcut
      val config = context.system.settings.config
      override val supervisorStrategy = OneForOneStrategy () {
        case _: ActorKilledException => Escalate
        case _: ActorInitializationException => Escalate
        case _ => Stop
      }

      override def preStart(){
        import scala.collection.JavaConverters._
        // Get our passenger names from the config
        val passengers = config.getList("zzz.akka.avionics.passengers")

        // Iterate through them to create the passenger
        passengers.asScala.foreach { nameWithSeat =>
        // Convert spaces to underscores to comply with URI standard
          val id = nameWithSeat.asInstanceOf[ConfigList]
            .unwrapped().asScala.mkString("-")
            .replaceAllLiterally(" ", "_")
          context.actorOf(newPassenger(callButton, bathrooms), id)
        }
      }

      def receive = {
        case GetChildren => sender ! context.children
      }
    }), "PassengerSupervisor")
  }

  implicit val askTimeout = Timeout(5.seconds)
  def noRouter: Receive = {
    case GetPassengerBroadcaster =>
      val destinedFor = sender
      val actor = (context.actorSelection("PassengerSupervisor") ? GetChildren)
      .mapTo[Iterable[ActorRef]] map { passengers =>
        val paths = passengers.map { p => p.path.toString}
        val props:Props = BroadcastGroup(paths).props()
        // Send this tuple to ourselves
        (props, destinedFor)
      } pipeTo self

    case (routerProps: Props, destinedFor: ActorRef) =>
      val router = context.actorOf(routerProps, "Passengers")
      destinedFor ! PassengerBroadcaster(router)
      context.become(withRouter(router))
  }


  def withRouter(router: ActorRef): Receive = {
    case GetPassengerBroadcaster => sender ! PassengerBroadcaster(router)
    case (_, destinedFor:ActorRef) => destinedFor ! PassengerBroadcaster(router)
  }

  def receive = noRouter

}