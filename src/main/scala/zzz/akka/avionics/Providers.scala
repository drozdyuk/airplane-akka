package zzz.akka.avionics

import akka.actor.{Props, ActorRef}

/** Helper traits with functions for creating pilots, flight attendants and altimeter.
  *
  * Takes care of certain dependencies, e.g. EventSourceImpl is used when creating an Altimeter.
  * Created by Andriy Drozdyuk on 05-Apr-14.
  */
trait PilotProvider {
  def newPilot(plane: ActorRef, autopilot: ActorRef, heading: ActorRef, altimeter: ActorRef): Props =
    Pilot.props(plane, autopilot, heading, altimeter)

  def newCopilot(plane: ActorRef, autopilot: ActorRef, altimeter: ActorRef): Props =
    Copilot.props(plane, autopilot, altimeter)

  def newAutopilot(plane: ActorRef): Props =
    Autopilot.props(plane: ActorRef)
}

trait LeadFlightAttendantProvider {
  def newLeadFlightAttendant: Props =
    LeadFlightAttendant.props()
}

trait AltimeterProvider {
  def newAltimeter: Props =
    Altimeter.props()
}

trait HeadingIndicatorProvider {
  def newHeadingIndicator: Props =
    HeadingIndicator.props()
}

trait DrinkingProvider {
  def newDrinkingBehaviour(drinker: ActorRef): Props =
    DrinkingBehaviour.props(drinker)
}

trait FlyingProvider {
  def newFlyingBehaviour(plane: ActorRef,
                         heading: ActorRef,
                         altimeter: ActorRef): Props =
    FlyingBehaviour.props(plane, heading, altimeter)
}
