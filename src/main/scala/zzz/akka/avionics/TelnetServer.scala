package zzz.akka.avionics

import akka.actor._
import akka.pattern.ask
import akka.io.{Tcp, IO}
import java.net.InetSocketAddress
import akka.io.Tcp.{Write, PeerClosed, Received}
import akka.util.{Timeout, ByteString}
import zzz.akka.avionics.Altimeter.{CurrentAltitude, GetCurrentAltitude}
import scala.concurrent.duration._
import zzz.akka.avionics.HeadingIndicator.{CurrentHeading, GetCurrentHeading}
import scala.util.{Failure, Success}

object TelnetServer {
  // Welcome message to show on connection
  val welcome =
    """|Welcome to the Airplane!
      |------
      |Valid commands are: 'heading' and 'altitude'
      |> """.stripMargin

  def altStr(alt: Double) = ByteString(f"Current altitude is $alt%5.2f feet\n\r\n> ")

  def headingStr(heading: Float) = ByteString(f"Current heading is $heading%3.2f degrees\n\r\n> ")

  def unknown(str: String) = ByteString(s"Current $str is unknown\n\r\n> ")

  // Decode bytestrings
  def ascii(bytes: ByteString): String = {
    bytes.decodeString("UTF-8").trim()
  }

  def props(plane: ActorRef, port: Int): Props = Props(new TelnetServer(plane, port))

  object ConnectionHandler {
    def props(): Props = Props[ConnectionHandler]
  }
  class ConnectionHandler extends Actor with Stash with ActorLogging {
    implicit val ec = context.dispatcher

    implicit val askTimeout = Timeout(1.second)
    def receive = uninitialized

    def uninitialized: Receive = {
      case plane: ActorRef =>
        unstashAll()
        context become initialized(plane)
      case PeerClosed => context stop self
      case _ => stash()
    }

    def initialized(plane: ActorRef): Receive = {
      case Received(_) =>
        sender ! Write(ByteString(TelnetServer.welcome))
        context become welcomed(plane)
      case PeerClosed => context stop self
    }

    def welcomed(plane: ActorRef): Receive = {
      case Received(data) =>
        // DO NOT close over the sender.
        val socket = sender()

        TelnetServer.ascii(data) match {
          case "altitude" =>
            (plane ? GetCurrentAltitude).mapTo[CurrentAltitude].onComplete {
              case Success(CurrentAltitude(alt)) => socket ! Write(altStr(alt))
              case Failure(t: Throwable) =>
                log.error(t, "Failed to get altitude.")
                socket ! Write(unknown("altitude"))
            }
          case "heading" =>
            (plane ? GetCurrentHeading).mapTo[CurrentHeading].onComplete {
              case Success(CurrentHeading(heading)) => socket ! Write(headingStr(heading))
              case Failure(t: Throwable) =>
                log.error(t, "Failed to get heading.")
                socket ! Write(unknown("heading"))
            }
          case _ => socket ! Write(ByteString("What?\n\r\n> "))
        }

      case PeerClosed => context stop self
    }

  }

}

/** Allows communication with the plane via remote commands.
 *
 * @param plane Reference to the plane actor
 * Created by Andriy Drozdyuk on 19-Apr-14.
 */
class TelnetServer(plane: ActorRef, port: Int) extends Actor
with ActorLogging {
  import Tcp._
  import context.system
  import TelnetServer._

  IO(Tcp) ! Bind(self, new InetSocketAddress("localhost", port))

  def receive = {
    case b @ Bound(localAddress) =>
      log.info("Plane Server Listening on {}", localAddress)

    // New client connected
    case c @ Connected(remote, local) =>
      log.info("New Connection Initiated from {}", remote)
      val handler = context.actorOf(ConnectionHandler.props())
      handler ! plane
      val connection = sender()
      connection ! Register(handler)

  }

}


