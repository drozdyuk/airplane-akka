package zzz.akka.avionics

import scala.util.Random
import scala.concurrent.duration._
import akka.actor.{Props, Cancellable, ActorRef, Actor}
import zzz.akka.avionics.FlightAttendant._
import zzz.akka.avionics.FlightAttendant.Assist
import zzz.akka.avionics.FlightAttendant.GetDrink
import zzz.akka.avionics.FlightAttendant.Drink
import scala.Some


// Trait to abstract creation of flight attendants
// away from the entity creating them. It makes
// it easier to mix in creation to the rest of the plane
trait FlightAttendantProvider {
  def newFlightAttendant(): Props =
    Props(new FlightAttendant with AttendantResponsiveness {
      val maxResponseTimeMS = 300000
    })
}


// Trait that allows us to create diff flight attendants
// with diff levels of responsiveness
trait AttendantResponsiveness {
  val maxResponseTimeMS: Int
  def responseDuration = Random.nextInt(maxResponseTimeMS).millis
}

object FlightAttendant {
  case class Assist(passenger: ActorRef)
  case object Busy_?
  case object Yes
  case object No
  case class GetDrink(drinkName: String)
  case class Drink(drinkName: String)

  // by default make attendants respond within 5 minutes
  def props(): Props = Props(new FlightAttendant with AttendantResponsiveness {
    val maxResponseTimeMS = 300000
  })
}

class FlightAttendant extends Actor {
  this: AttendantResponsiveness =>
  // ame exec context implicit for scheduler
  implicit val ec = context.dispatcher

  // Internal message we can use to signal that drink deliver can take place
  case class DeliverDrink(drink: Drink)

  // Stores our time, which is an instance of 'Cancellabe'
  var pendingDelivery: Option[Cancellable] = None

  // Just makes scheduling a delivery a bit simpler
  def scheduleDelivery(drinkName: String): Cancellable = {
    context.system.scheduler.scheduleOnce(responseDuration, self, DeliverDrink(Drink(drinkName)))
  }

  // If we have an injured passenger, then we need to immediately assist them, by giving them
  // the 'secret' Magic Healing Potion that's available on all flights in and out of Xanadu
  def assistInjuredPassenger: Receive = {
    case Assist(passenger) =>
      // It's an emergency... stop what we're doing and assist NOW
      pendingDelivery foreach { _.cancel() }
      pendingDelivery = None
      passenger ! Drink("Magic Healing Potion")
  }

  // This general handler is responsible for servicing drink
  // requests when we're not busy servicing an existing request
  def handleDrinkRequests: Receive = {
    case GetDrink(drinkName) =>
      pendingDelivery = Some(scheduleDelivery(drinkName))
      // Become something new
      context.become(assistInjuredPassenger orElse handleSpecificPerson(sender))

    case Busy_? => sender ! No
  }

  // When we are already busy getting a drink for someone
  // then we move to this state
  def handleSpecificPerson(person: ActorRef): Receive = {
    // If the person asking us for a drink is the same person
    // we're currently handling then we'll cancel what we were doing
    // and service their new request
    case GetDrink(drinkName) if sender == person =>
      pendingDelivery foreach {_.cancel()}
      pendingDelivery = Some(scheduleDelivery(drinkName))

    // The only time we can get the DeliverDrink message is
    // when we're in this state
    case DeliverDrink(drink) =>
      person ! drink
      pendingDelivery = None
      // Become something new
      context.become(assistInjuredPassenger orElse handleDrinkRequests)

    // If we get another drink request when we're already
    // handling one then we punt that back to our parent
    // (the LeadFlightAttendant)
    case m: GetDrink =>
      context.parent forward m

    case Busy_? => Yes
  }


  // Setup the initial handler
  def receive = assistInjuredPassenger orElse handleDrinkRequests

}
