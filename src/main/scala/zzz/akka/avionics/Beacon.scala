package zzz.akka.avionics

import scala.concurrent.duration._
import akka.actor.{Props, Actor}

/**
 * Supplies properties for behavior of beacons at airports.
 * Created by Andriy Drozdyuk on 27-Apr-14.
 */
trait BeaconResolution {
  lazy val beaconInterval = 1.second
}

trait BeaconProvider {
  // factory method for creating a beacon
  def newBeacon(heading: Float) = Beacon.props(heading)
}
object Beacon {

  /**
   * Message that is sent from the Beacon to its listener
   * @param heading What the listener's heading should be to reach the beacon.
   */
  case class BeaconHeading(heading: Float)

  def props(heading: Float): Props = Props(new Beacon(heading) with BeaconResolution)
}

/**
 * Beacon will continually emit the BeaconHeading message with the given
 * heading constructor param.
 * @param heading The heading to emit
 */
class Beacon(heading: Float) extends  Actor {
  this: BeaconResolution =>
  import Beacon._
  import EventSource._

  case object Tick

  // Specialized event bus for handling pub/sub
  val bus = new EventBusForActors[BeaconHeading, Boolean]({
    _: BeaconHeading => true
  })

  implicit val ec = context.dispatcher
  val ticker = context.system.scheduler.schedule(beaconInterval, beaconInterval, self, Tick)

  def receive = {
    case RegisterListener(actor) =>
      bus.subscribe(actor, true)
    case UnregisterListener(actor) =>
      bus.unsubscribe(actor)
    case Tick =>
      bus.publish(BeaconHeading(heading))
  }
}


