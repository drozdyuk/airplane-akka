package zzz.akka.avionics

import akka.actor.{Actor, Props, ActorRef}
import akka.actor.Actor.Receive
import zzz.akka.avionics.Airport.{StopDirectingFlyer, DirectFlyerToAirport}
import zzz.akka.avionics.Beacon.BeaconHeading
import zzz.akka.avionics.FlyingBehaviour.{CourseTarget, Fly}

trait AirportSpecifics {
  lazy val headingTo: Float = 0.0f
  lazy val altitude: Double = 0
}

object Airport {
  // Messages consumed by Airport
  case class DirectFlyerToAirport(flyingBehaviour: ActorRef)
  case class StopDirectingFlyer(flyingBehaviour: ActorRef)

  // Factory method to instantiate the Toronto International
  // Airport
  def toronto(): Props =  Props(new Airport
    with BeaconProvider
    with AirportSpecifics {
    override lazy val headingTo: Float = 314.3f
    override lazy val altitude: Double = 26000
  })
}

/**
 * Airport actor
 * Created by Andriy Drozdyuk on 28-Apr-14.
 */
class Airport extends Actor {
  this: AirportSpecifics with BeaconProvider =>

  // Our beacon, which periodically sends out our airport's
  // heading
  val beacon = context.actorOf(Beacon.props(headingTo), "Beacon")

  override def receive: Receive = {
    // FlyingBehaviour instances subscribe to this Airport in
    // order to be told where they should be flying
    case DirectFlyerToAirport(flyingBehaviour) =>
      val oneHourFromNow = System.currentTimeMillis() + 60 * 60 * 1000
      val when = oneHourFromNow

      // But, we can't let them get BeaconHeading messages,
      // since those are not understood by FlyingBehaviour instances.
      // We need to transform those messages into appropriate
      // fly messages
      context.actorOf(Props(
        new MessageTransformer(from = beacon,
        to = flyingBehaviour, {
          case BeaconHeading(heading) => Fly(CourseTarget(altitude, heading, when))
        }
        )))

    case StopDirectingFlyer(_) => context.children.foreach { context.stop}
      
  }
}
