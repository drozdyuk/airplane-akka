package zzz.akka.avionics

import akka.actor.{Props, FSM, Actor, ActorRef}
import zzz.akka.avionics.Plane.{LostControl, Controls, GiveMeControl}
import zzz.akka.avionics.EventSource.{UnregisterListener, RegisterListener}
import scala.concurrent.duration._
import zzz.akka.avionics.HeadingIndicator.HeadingUpdate
import zzz.akka.avionics.Altimeter.AltitudeUpdate
import zzz.akka.avionics.Pilot.RelinquishControl

/** FSM implementation of the plane flying behaviour.
 * Created by Andriy Drozdyuk on 09-Apr-14.
 */
object FlyingBehaviour {
  import ControlSurfaces._

  def props(plane: ActorRef, heading: ActorRef, altimeter: ActorRef): Props =
    Props(new FlyingBehaviour(plane, heading, altimeter))

  // The states governing behavioural transitions
  sealed trait State
  case object Idle extends State
  case object Flying extends State
  case object PreparingToFly extends State

  // Data we can use - helper classes to hold course data
  case class CourseTarget(altitude: Double, heading: Float, byMillis: Long)
  case class  CourseStatus(altitude: Double, heading: Float, headingSinceMS: Long,
                            altitudeSinceMS:Long)

  // We're going to allow FSM to vary the behavior that calculates the control changes
  // using this function def
  type Calculator = (CourseTarget, CourseStatus) => Any

  // The Data that our FlyingBehaviour can hold
  sealed trait Data
  case object Uninitialized extends Data

  // This is the 'real' data. We're going to stay entirely
  // immutable and, in doing so, we're going to encapsulate
  // all of the changing state data inside this class
  case class FlightData(controls: ActorRef,
                        elevCalc: Calculator,
                        bankCalc: Calculator,
                        target: CourseTarget,
                        status: CourseStatus) extends Data

  // Someone can tell the FlyingBehaviour to fly
  case class Fly(target: CourseTarget)

  // Some sets a new calculator to use
  case class NewElevatorCalculator(calc: Calculator)
  case class NewBankCalculator(calc: Calculator)


  def currentMS = System.currentTimeMillis

  // Calculates the amount of elevator change we need to make
  // and returns it
  def calcElevator(target: CourseTarget,
                    status: CourseStatus): Any = {
    val alt = (target.altitude - status.altitude).toFloat
    val dur = target.byMillis - status.altitudeSinceMS
    if (alt < 0) StickForward((alt/dur) * -1)
    else StickBack(alt / dur)
  }

  // Calculates the amount of bank change we need to make
  // and returns it
  def calcAilerons(target: CourseTarget, status: CourseStatus): Any = {
    import scala.math.{abs,signum}
    val diff = target.heading - status.heading
    val dur = target.byMillis - status.headingSinceMS
    val amount = if (abs(diff) < 180) diff
                 else signum(diff) * (abs(diff) - 360f)
    if (amount > 0) StickRight(amount / dur)
    else StickLeft((amount / dur) * -1)
  }

}

class FlyingBehaviour(plane: ActorRef,
                      heading: ActorRef,
                      altimeter: ActorRef) extends Actor
with FSM[FlyingBehaviour.State, FlyingBehaviour.Data] {
  import FlyingBehaviour._
  // Internal message to tell Flying Behaviour that we need to adjust
  // plane's altitude and heading
  case object Adjust

  def adjust(flightData: FlightData): FlightData = {
    val FlightData(controls, elevCalc, bankCalc, target, status) = flightData
    controls ! elevCalc(target, status)
    controls ! bankCalc(target, status)
    flightData
  }

  def prepComplete(data: Data): Boolean = {
    data match {
      case FlightData(c, _, _, _, s) =>
        if (!c.isTerminated && s.heading != -1f && s.altitude != -1f)
          true
        else
          false
      case _ =>
        false
    }
  }


  // Internal message to tell Flying Behaviour that we can't get
  // the necessary equipment
  case object NotAcquired

  // Sets up the initial values for state and data in the FSM
  startWith(Idle, Uninitialized)

  when(Idle) {
    case Event(Fly(target), _) =>
      goto(PreparingToFly) using FlightData(
        context.system.deadLetters,
        calcElevator,
        calcAilerons,
        target,
        CourseStatus(-1, -1, 0, 0))
  }

  onTransition {
    case Idle -> PreparingToFly =>
      plane ! GiveMeControl
      heading ! RegisterListener(self)
      altimeter ! RegisterListener(self)
      setTimer("Acquisition", NotAcquired, 5.seconds, repeat = false)
  }

  when (PreparingToFly)(transform {
    case Event(HeadingUpdate(head), d: FlightData) =>
       stay using d.copy(status =
         d.status.copy(heading = head, headingSinceMS = currentMS))

    case Event(AltitudeUpdate(alt), d: FlightData) =>
       stay using d.copy(status =
         d.status.copy(altitude = alt, altitudeSinceMS = currentMS))

    case Event(Controls(ctr), d: FlightData) =>
        stay using d.copy(controls = ctr)

    case Event(NotAcquired, _) =>
      plane ! LostControl
      goto (Idle)
  } using {
    // Move on to the Flying state
    case s if prepComplete(s.stateData) => s.copy(stateName = Flying)
  })

  onTransition {
    case Flying -> _ =>
      cancelTimer("Acquisition")
  }

  onTransition {
    case PreparingToFly -> Flying =>
      setTimer("Adjustment", Adjust, 200.milliseconds, repeat = true)
  }

  when(Flying) {
    case Event(AltitudeUpdate(alt), d: FlightData) =>
      stay using d.copy(status =
      d.status.copy(altitude = alt, altitudeSinceMS = currentMS))
    case Event(HeadingUpdate(head), d: FlightData) =>
      stay using d.copy(status =
      d.status.copy(heading = head, headingSinceMS = currentMS))
    case Event(Adjust, flightData: FlightData) =>
      stay using adjust(flightData)
    case Event(NewElevatorCalculator(calc), d: FlightData) =>
      stay using d.copy(elevCalc = calc)
    case Event(NewBankCalculator(calc), d: FlightData) =>
      stay using d.copy(bankCalc = calc)
  }

  onTransition {
    case Flying -> _ =>
      cancelTimer("Adjustment")
  }

  onTransition {
    case _ -> Idle =>
      heading ! UnregisterListener(self)
      altimeter ! UnregisterListener(self)
  }

  whenUnhandled {
    case Event(RelinquishControl, _) =>
      goto(Idle)
  }

  initialize
}




