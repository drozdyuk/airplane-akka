package zzz.akka.avionics.tests

import zzz.akka.avionics.FlightAttendant.{Drink, GetDrink}
import zzz.akka.avionics.{AttendantResponsiveness, FlightAttendant}
import akka.testkit.{TestActorRef, ImplicitSender, TestKit}
import akka.actor.{Props, ActorSystem}
import org.scalatest.{Matchers, WordSpecLike}

/** Make sure flight attendants work as expected.
 * Created by Andriy Drozdyuk on 05-Apr-14.
 */
object TestFlightAttendant {
  def apply() = new FlightAttendant with AttendantResponsiveness {
    val maxResponseTimeMS = 1
  }
}

// CAN'T DO THIS ON WINDOWS - min is 10 ms
// ConfigFactory.parseString("akka.scheduler.tick-duration = 1ms")
class FlightAttendantSpec extends TestKit(ActorSystem("FlightAttendantSpec"))
with ImplicitSender
with WordSpecLike
with Matchers
{
  // TODO:  THIS RUNS SLOWER THAN EXPECTED!!! Schedule is to blame
  "Flight Attendant" should {
    "get a drink when asked" in {
      val a = TestActorRef(Props(TestFlightAttendant()))
      a ! GetDrink("soda")
      expectMsg(Drink("soda"))
    }
  }

}
