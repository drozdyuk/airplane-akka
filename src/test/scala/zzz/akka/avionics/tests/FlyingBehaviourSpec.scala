package zzz.akka.avionics.tests

import akka.testkit.{TestFSMRef, TestProbe, ImplicitSender, TestKit}
import akka.actor.{ActorRef, ActorSystem}
import com.typesafe.config.ConfigFactory
import org.scalatest.{Matchers, WordSpecLike}
import zzz.akka.avionics.FlyingBehaviour
import zzz.akka.avionics.FlyingBehaviour._
import zzz.akka.avionics.HeadingIndicator.HeadingUpdate
import zzz.akka.avionics.FlyingBehaviour.Fly
import zzz.akka.avionics.HeadingIndicator.HeadingUpdate
import scala.util.Random
import zzz.akka.avionics.Altimeter.AltitudeUpdate
import zzz.akka.avionics.Plane.Controls

/** Test the flying behaviour.
 * Created by Andriy Drozdyuk on 12-Apr-14.
 */
object FlyingBehaviourSpec {

}
class FlyingBehaviourSpec extends TestKit(ActorSystem("FlyingBehaviourSpec"))
with ImplicitSender with WordSpecLike with Matchers{
  import FlyingBehaviourSpec._

  // Create an "empty" Actor as TestProbe.
  def nilActor: ActorRef = TestProbe().ref

  val target = CourseTarget(Random.nextInt(10), Random.nextInt(100), Random.nextInt(1000))

  def fsm(plane: ActorRef = nilActor,
          heading: ActorRef = nilActor,
          altimeter: ActorRef = nilActor) = {
    TestFSMRef(new FlyingBehaviour(plane, heading, altimeter))
  }

  "FlyingBehaviour" should {
    "start in the Idle state and with Uninitialized data" in {
      val a = fsm()
      a.stateName should be (Idle)
      a.stateData should be (Uninitialized)
    }
  }

  "PreparingToFly state" should {
    "stay in PreparingToFly after HeadingUpdate is received" in {

      val a = fsm()
      a ! Fly(target)
      a ! HeadingUpdate(20)
      a.stateName should be (PreparingToFly)
      val sd = a.stateData.asInstanceOf[FlightData]
      sd.status.altitude should be (-1)
      sd.status.heading should be (20)

    }

    "move to Flying when all parts are received" in {
      val a = fsm()
      a ! Fly(target)
      a ! HeadingUpdate(20)
      a ! AltitudeUpdate(20)
      a ! Controls(testActor)
      a.stateName should be (Flying)
      val sd = a.stateData.asInstanceOf[FlightData]
      sd.controls should be (testActor)
      sd.status.altitude should be (20)
      sd.status.heading should be (20)
    }
  }

  "transitioning to Flying state" should {
    "create the Adjustment timer" in {
      val a = fsm()
      a.setState(PreparingToFly)
      a.setState(Flying)
      a.isTimerActive("Adjustment") should be (true)
    }
  }

}
