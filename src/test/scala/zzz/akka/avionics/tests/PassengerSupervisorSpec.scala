package zzz.akka.avionics.tests

import com.typesafe.config.ConfigFactory
import zzz.akka.avionics.{PassengerSupervisor, PassengerProvider}
import akka.actor.{Props, ActorSystem, Actor, ActorRef}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike, Matchers}
import akka.testkit.{TestKit, ImplicitSender}
import zzz.akka.avionics.PassengerSupervisor.{PassengerBroadcaster, GetPassengerBroadcaster}
import scala.concurrent.duration._

/** Test for the passenger supervisor.
 * Created by Andriy Drozdyuk on 16-Apr-14.
 */

object PassengerSupervisorSpec {
  val config = ConfigFactory.parseString(
    """
      zzz.akka.avionics.passengers = [
             [ "Kelly Franqui",   "23", "A" ],
             [ "Tyrone Dotts",    "23", "B" ],
             [ "Malinda Class",   "23", "C" ],
             [ "Kenya Jolicoeur", "24", "A" ],
             [ "Christian Piche", "24", "B" ]
         ]
    """.stripMargin)

}

// We won't work with "real" passengers. This mock
// passenger will be much easier to verify things with
trait TestPassengerProvider extends PassengerProvider {
  override def newPassenger(callButton: ActorRef, bathrooms: ActorRef): Props = {
    Props(new Actor {
      def receive = {
        case m => callButton ! m
      }
    })
  }
}

class PassengerSupervisorSpec extends TestKit(ActorSystem("PassengerSupervisorSpec",
  PassengerSupervisorSpec.config))
with ImplicitSender
with WordSpecLike
with BeforeAndAfterAll
with Matchers {
  // Clean up the system when all the tests are done
  override def afterAll() {
    system.shutdown()
  }

  "PassengerSupervisor" should {
    "work" in {
      // Get our SUT
      val a = system.actorOf(Props(new PassengerSupervisor(testActor, testActor) with TestPassengerProvider))
      // Grab the BroadcastRouter
      a ! GetPassengerBroadcaster

      val broadcaster = expectMsgType[PassengerBroadcaster].broadcaster
      broadcaster ! "Hithere"
      // All 5 passengers should say "Hithere"
      expectMsg("Hithere")
      expectMsg("Hithere")
      expectMsg("Hithere")
      expectMsg("Hithere")
      expectMsg("Hithere")
      // And then nothing else
      expectNoMsg(100.milliseconds)
      // Ensure that the cache works
      a ! GetPassengerBroadcaster
      expectMsg(PassengerBroadcaster(`broadcaster`))
    }
  }
}
