package zzz.akka.avionics.tests

import akka.actor.{ActorSystem, Actor}
import zzz.akka.avionics.EventSource.{UnregisterListener, RegisterListener}
import zzz.akka.avionics.EventSourceImpl
import akka.testkit.{TestActorRef, TestKit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

/** Test that EventSource is working as intended.
 * Created by Andriy Drozdyuk on 4/4/14.
 */
// Create a specific event source derivation that conforms
// to req. of the trait
class TestEventSource extends Actor with EventSourceImpl{
  def receive = eventSourceReceive
}

class EventSourceSpec extends TestKit(ActorSystem("EventSourceSpec"))
with WordSpecLike
with Matchers
with BeforeAndAfterAll {
  override def afterAll(){system.shutdown()}

  "EventSource" should {
    "allow us to register a listener" in {
      val actor = TestActorRef[TestEventSource].underlyingActor
      actor.receive(RegisterListener(testActor)) // testActor provided by TestKit
      actor.listeners should contain (testActor)
    }

    "allow us to unregister a listener" in {
      val actor = TestActorRef[TestEventSource].underlyingActor
      actor.receive(RegisterListener(testActor))
      actor.receive(UnregisterListener(testActor))
      actor.listeners.size should be (0)
    }

    "send the event to our test actor" in {
      val actor = TestActorRef[TestEventSource]
      actor ! RegisterListener(testActor) // could  have used .receive too
      actor.underlyingActor.sendEvent("Fibonacci") // send to all subscribers
      expectMsg("Fibonacci")
    }

  }
}

