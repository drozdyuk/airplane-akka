package zzz.akka.avionics.tests

import akka.actor._
import org.scalatest.{Matchers, WordSpecLike}
import akka.testkit.{TestProbe, ImplicitSender, TestKit}
import zzz.akka.avionics.{TelnetServer, Altimeter, HeadingIndicator}
import akka.io.{Tcp, IO}
import akka.io.Tcp._
import java.net.InetSocketAddress
import akka.util.ByteString
import akka.io.Tcp.Connected
import akka.io.Tcp.Received
import akka.io.Tcp.Register
import akka.io.Tcp.Connect
import scala.concurrent.duration._

class PlaneForTelnet extends Actor with ActorLogging{
  import HeadingIndicator._
  import Altimeter._
  def receive = {
    case GetCurrentAltitude => sender ! CurrentAltitude(123.0f)
    case GetCurrentHeading => sender ! CurrentHeading(43.1f)

  }
}


class TelnetServerSpec extends TestKit(ActorSystem("PlaneServerSpec"))
  with ImplicitSender
  with WordSpecLike
  with Matchers {

  "PlaneServer" should {
    "work" in {
      val p = system.actorOf(Props[PlaneForTelnet], "Plane")
      val s = system.actorOf(TelnetServer.props(p, 31777), "Telnet")

      IO(Tcp) ! Connect(new InetSocketAddress("localhost", 31777))

      val testProbe = TestProbe()

      expectMsgType[Connected]

      // The actor who sent Connected message above is our personal tcp actor (i.e. socket)
      val socket = lastSender
      socket ! Register(testProbe.ref)

      // Telnet protocol seems to expect some kind of random "hello" message from the client
      // Emulate telnet connection by sending random stuff and expect a welcome message
      socket ! Write(ByteString("blah blah"))
      testProbe.expectMsgPF() {
        case Received(data) =>
          val result = TelnetServer.ascii(data)
          result should include ("Welcome to the Airplane!")
      }

      // Test altitude command
      socket ! Write(ByteString("altitude"))

      testProbe.expectMsgPF() {
        case Received(data) =>
          val result = TelnetServer.ascii(data)
          result should include ("123.00 feet")
      }

      // Test heading command
      socket ! Write(ByteString("heading"))
      testProbe.expectMsgPF() {
        case Received(data) =>
          val result = TelnetServer.ascii(data)
          result should include ("43.10 degrees")
      }

      socket ! Close
    }
  }

}

