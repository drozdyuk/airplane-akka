package zzz.akka.avionics.tests

import akka.testkit.{TestActorRef, TestLatch, ImplicitSender, TestKit}
import akka.actor.{Props, Actor, ActorSystem}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import zzz.akka.avionics.Altimeter.{AltitudeUpdate, RateChange}
import zzz.akka.avionics.{Altimeter, EventSource}
import scala.concurrent.Await
import scala.concurrent.duration._

/** Make sure altimeter works as expected.
 * Created by Andriy Drozdyuk on 4/4/14.
 */
class AltimeterSpec extends TestKit(ActorSystem("AltimeterSpec"))
with ImplicitSender
with WordSpecLike
with Matchers
with BeforeAndAfterAll
{
  override def afterAll(){system.shutdown()}

  // Instantiate a helper class for every test
  class Helper {
    object EventSourceSpy {
      // Latch gives us feedback when something happens
      var latch = TestLatch(1)
    }

    // Our special derivation of EventSource gives us
    // hooks into concurrency
    trait EventSourceSpy extends EventSource {
      override def sendEvent[T](event: T): Unit = {
        EventSourceSpy.latch.countDown()
      }
        // don't care about processing message
      override def eventSourceReceive = Actor.emptyBehavior
    }

    // Constructs our altimeter with EventSourceSpy
    def slicedAltimeter = new Altimeter with EventSourceSpy

    // Helper to give us ActorRef and plain Altimeter we can work on
    def refAndActor() = {
      val a = TestActorRef[Altimeter](Props(slicedAltimeter))
      (a, a.underlyingActor)
    }
  }

  "Altimeter" should {
    "record rate of climb changes" in new Helper {
      val (_, actor) = refAndActor()
      actor.receive(RateChange(1f))
      actor.rateOfClimb should be (actor.maxRateOfClimb)
    }

    "keep rate of climb changes within bounds" in new Helper {
      val (_, actor) = refAndActor()
      actor.receive(RateChange(2f))
      actor.rateOfClimb should be (actor.maxRateOfClimb)
    }

    "calculate altitude change" in new Helper {
      val ref = system.actorOf(Altimeter.props())
      ref ! EventSource.RegisterListener(testActor)
      ref ! RateChange(1f)
      // run passed in partial function repeatedly as long as it returns false
      fishForMessage() {
        case AltitudeUpdate(altitude) if altitude == 0f =>
          false
        case AltitudeUpdate(altitude) =>
          true
      }
    }

    "send events" in new Helper {
      val (ref, _) = refAndActor()
      Await.ready(EventSourceSpy.latch, 1.second)
      EventSourceSpy.latch.isOpen should be (true)

    }

  }




}
