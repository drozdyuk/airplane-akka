package zzz.akka.avionics.tests

import zzz.akka.avionics.{Passenger, DrinkRequestProbability}
import scala.concurrent.duration._
import akka.testkit.{TestProbe, ImplicitSender, TestKit}
import akka.actor.{Props, ActorRef, ActorSystem}
import akka.event.Logging.Info
import zzz.akka.avionics.Passenger.FastenSeatbelts
import org.scalatest.{WordSpecLike, Matchers, WordSpec}

/** Test for the passengers.
 * Created by Andriy Drozdyuk on 14-Apr-14.
 */

trait TestDrinkRequestProbability extends DrinkRequestProbability {
  override val askThreshold = 0f
  override val requestMin = 0.millis
  override val requestUpper = 2.millis
}

class PassengersSpec extends TestKit(ActorSystem())
with WordSpecLike
with Matchers
with ImplicitSender {

  var seatNumber = 9
  def newPassenger(): ActorRef = {
    seatNumber += 1
    system.actorOf(Props(new Passenger(testActor, testActor) with TestDrinkRequestProbability),
      s"Pat_Metheny-$seatNumber-B")
  }

  "Passengers" should {
    "fasten seatbelts when asked" in {
      val a = newPassenger()
      val p = TestProbe()
      system.eventStream.subscribe(p.ref, classOf[Info])
      a ! FastenSeatbelts
      p.expectMsgPF() {
        case Info(_, _, m) => m.toString should include ("fastening seatbelt")
      }
    }
  }



}
