package zzz.akka.avionics.tests

import akka.testkit._
import akka.actor._
import org.scalatest.{Matchers, WordSpecLike}
import akka.pattern.ask
import zzz.akka.avionics._
import scala.concurrent.Await
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import akka.util.Timeout
import zzz.akka.avionics.Plane.{CopilotReference, RequestCopilot, GiveMeControl}
import zzz.akka.avionics.Pilot.ReadyToGo
import zzz.akka.avionics.Copilot.PilotAssigned
import java.util.concurrent.TimeUnit

/** Pilot and Copilot tests.
 * Created by Andriy Drozdyuk on 4/4/14.
 */

class FakePilot extends Actor {
  override def receive = {
    case _ =>
  }
}

object PilotsSpec {
  val copilotName = "Mary"
  val pilotName = "Mark"
  val configStr: String = s"""
  zzz.akka.avionics.flightCrew.copilotName = "$copilotName"
  zzz.akka.avionics.flightCrew.pilotName = "$pilotName"
  """

}



class PilotsSpec extends TestKit(ActorSystem("PilotsSpec", ConfigFactory.parseString(PilotsSpec.configStr))) with ImplicitSender
with WordSpecLike
with Matchers {

  import PilotsSpec._

  // The 'ask' and 'await' timeouts
  implicit val timeout = Timeout(4.seconds)

  // Create an "empty" Actor as TestProbe.
  def nilActor: ActorRef = TestProbe().ref

  // Paths for actors
  val pilotPath = s"/user/TestPilots/$pilotName"
  val copilotPath = s"/user/TestPilots/$copilotName"

  // Construct the hierarchy we need and ensure that the children
  // are good to go by the time we're done
  def pilotsReadyToGo(): ActorRef = {

    val a = system.actorOf(
      Props(new IsolatedStopSupervisor with OneForOneStrategyFactory {

        override def childStarter(): Unit = {
          val pilot = context.actorOf(Props[FakePilot], pilotName)
          val copilot = context.actorOf(Copilot.props(testActor, nilActor, nilActor),
            copilotName)
          copilot ! PilotAssigned(pilot)

        }
      }), "TestPilots")

    // Wait for mailboxes to be up and running for the children
    // TODO: Will this work with Identify method?
    Await.result(a ? IsolatedLifeCycleSupervisor.WaitForStart, 3.seconds)

    // Tell the Copilot that it's ready to go
    system.actorSelection(copilotPath) ! ReadyToGo
    a
  }

  /**
   * Block and retrieve an actor at a particular actor path.
   * @param actorPath
   * @return actor ref
   */
  def actorBlockAndGet(actorPath: String): ActorRef = {
    Await.result(system.actorSelection(actorPath).resolveOne(), 1.seconds)
  }

  "Copilot" should {
    "take control when the Pilot dies" in {
      pilotsReadyToGo()
      // kill the pilot
      val pilot = actorBlockAndGet(pilotPath)
      pilot ! PoisonPill

      // Since the test class is the "Plane" we can expect to see this req.
      expectMsg(GiveMeControl)

      // The person who sent it better be copilot
      lastSender should be(actorBlockAndGet(copilotPath))
    }
  }

  "Autopilot" should {
    "take control when the Copilot dies" in {
      // The 'ask' below needs timeout
      implicit val askTimeout = Timeout(4.seconds)

      val copilot = TestActorRef[FakePilot]
      val autopilot = system.actorOf(Props(new Autopilot(testActor)))

      // Tell autopilot pilots are in position
      autopilot ! ReadyToGo

      // Expect msg sent by autopilot
      expectMsg(RequestCopilot)

      // Send a reference to copilot
      autopilot ! CopilotReference(copilot)

      // kill the copilot
      system.stop(copilot)


      expectMsg(GiveMeControl)
      // The sender better be autopilot
      lastSender should be(autopilot)
    }
  }
}
