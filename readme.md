Airplane Akka
--------------------

![airplane.jpg](http://i57.tinypic.com/2mebzw5.jpg)

Airplane simulation implemented in Akka, as laid out in the
book Akka Concurrency by Derek Wyatt (2013, Artima Press).

What is this?
=============
This code is simply my own implementation of the Airplane simulation example
given as a use case by [Akka Concurrency](http://www.artima.com/shop/akka_concurrency) book. I release it here in hopes
that it might prove helpful to someone.

Please note that while I am not actively supporting this repo, if you issue a
pull request that you feel fixes some serious error or adds a missing feature,
I will be happy to merge it!


Differences from the book
==========================

This implementation is meant to bring the book's outdated version of
Akka up to date. See the `build.sbt` file for the version that
is currently supported.

There are many differences between the book and the code you will find here.
Some arise from the deprecated features (e.g. actorFor), others from completely revamped
approaches (e.g. networking), while others are there simply to make
things work. To expand on the last point, the book leaves out a lot of code
and while easy to guess what the code should be approximately, actually
implementing it sometimes made me restructure a few things. Your version might be different.

The code represents the final state of the simulation, and is not broken down
by chapters. So, for example, if the book presents one way of doing something
and later overrides it with another (more correct way) --- you will only see
the latter case here. You can find the older versions if you dig through source control history,
but I don't recommend doing it that way. In my opinion, it is best to follow the book and only
peek here when you get stuck (or are just curious if there is another way to do
things).

Akka is a rapidly developing platform, so by the time you read this,
it might already be outdated. Keep up with the latest by reading the official
Akka docs, asking questions on StackOverflow, and bugging important people responsible
for its development ;-)

Broken Stuff
============

Some stuff is broken. Mostly the stuff towards the end of the book.

Contact
=======
The best way to contact me about a problem is to [create an issue](https://bitbucket.org/drozdyuk/airplane-akka/issues?status=new&status=open). I can also be reached by email at drozzy@gmail.com.

Acknowledgements
=================
I would like to thank Dan Luu, who provided the code over at https://github.com/danluu/akka-concurrency-wyatt
which also follows the book, while using a slightly older version of Akka. Derek Wyatt himself,
who can be reached via Twitter as [@derekwyatt](https://twitter.com/derekwyatt) --- took some time to point me in the right direction.
Lastly, the amazing users over at StackOverflow for putting up with my numerous, sometimes silly, [questions][so]


[so]: http://tinyurl.com/l7vtckq